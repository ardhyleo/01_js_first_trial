// 1
const club = ["Manchester United", "PSG", "Real Madrid", "Juventus", "Bayern Munchen"];

//2
const team = {
  name: "Manchester City",
  coach: "Pep Guardiola",
  stadium: "Etihad",
  owner: "Syeikh Mansour",
  superstar: {
    asname: "Kevin De Bruyne",
    from: "Belgium",
  },
};

//3
const { name, coach, stadium, owner, superstar } = team;
const { asname, from } = superstar;
const [one, two, three, four, five] = club;

console.log(one, two, three, four, five);
console.log(name, coach, stadium, owner);
console.log(asname, from);

//4
console.log(`this is my fans club ${club[0]} #GGMU`);

//5
const member = true;
const tipe = member === false ? "Bayar Rp. 150.000" : "Kamu Bayar lebih ya";
console.log(tipe);

//6
const clubs = [
  { id: 1, name: "Manchester United", point: 100 },
  { id: 2, name: "Manchester City", point: 95 },
  { id: 3, name: "Liverpool", point: 80 },
  { id: 4, name: "Tottenham Hotspur", point: 65 },
  { id: 5, name: "Arsenal", point: 50 },
];

//7
clubs.map((tim) => {
  console.log(`${tim.name} - ${tim.point + 100}`);
});

//8
const data = clubs.filter((club) => club.point >= 50);
console.log(data);

//9
const materi = clubs.find((club) => club.point === 100);
console.log(materi);
