// hoisting, proses pengangkatan variabel dari block ke global
/* perbedaan var, let & const
1. Si var dia di hoisting
2. Si var & si let dia bisa di re assign (dikasi nilai tinggi)

note : Hoisting, proses pengangkatan variabel ke global
*/

// var, let, const

// var nama1 = "MasTintin";
// let nama2 = "Mas Rafie";
// const nama3 = "Mas Zaidan";

// nama1 = "Mas Ardy";
// nama2 = "Mba Aini";
// nama3 = "Mas Tintin";

// function runNama() {
//     console.log(nama1);
//     console.log(nama2);
// }
// runNama()

// const nilai1 = "5";
// const nilai2 = "10";
// const isMarried = true;
// const hobby = ['Ngoding', 11, 'Main Hape'];

// console.log(hobby[0])

// if(nilai1 > nilai2  || nilai1 === 10)
// {
//     console.log("ok");
// }

// if(isMarried) {
//     console.log(`Mas ${nama1} & Mas ${nama2} udah Married`);
// } else if(nama1 < nama2) {
//     console.log('Tidak sama');
// } else {
//     console.log('5 lebih kecil dari 10')
// }

/*
Di dalam kurung melengkung function dia ada yang namanya argument, 
argument ini dia bakal nerima yang namanya parameter
Perbedaan function declaration dan arrow
*/
// Declaration Function
// function cetakNama (nama, age){
//     console.log(`nama saya ${nama}, umur ${age}`);
// }

// cetakNama('Ardi', 27);

// const biodata1 = {
//     nama: 'Mba Nisa',
//     umur: 23
// }

// // Arrow
// const cetakJodoh = (biodata) => {
//     console.log(biodata.nama);
//     console.log(biodata.umur);
//     console.log(`Mas Ardi ternyata berjodoh dengan ${biodata.nama} ${biodata.umur}`);
// }

//  cetakJodoh(biodata1)

//  const penambahanAngka = (a, b) => {
//     return a * b;
//   }

//   const result = penambahanAngka(10, 10);
//   console.log(result)
//   console.log("HAI ARDHYLEO")

// const nama = prompt("Masukkan Nama Anda");
// const umur = prompt("Masukkan Umur Anda");

// function register(nama, umur) {
//   if (name !== "ArdhyLeo") {
//     alert("Oke Mantap Saayyy!!");
//   } else if (age !== "30") {
//     alert("Oke Umur Cocok");
//   } else {
//     return `Selamat! Perndaftaran Anda Berhasil. Data Anda : ${nama},
//     ${umur}`;
//   }
// }

// function register(nama, umur) {
//   return `Selamat! Perndaftaran Anda Berhasil. Data Anda : ${nama},
//     ${umur}`;
// }
// const result = register(nama, umur);
// alert(result);
